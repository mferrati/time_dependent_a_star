/*********************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Simone Nardi, Hamal Marino, Mirko Ferrati, Centro di Ricerca "E. Piaggio", University of Pisa
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef MANIPULATION_PLANNER_H
#define MANIPULATION_PLANNER_H

#include <dual_manipulation_planner/graph_creator.h>
#include "planner.h"
#include <dual_manipulation_shared/planner_item.h>

class manipulation_planner : public dual_manipulation::planner::graphCreator, public Planner
{
public:
    manipulation_planner();
    
    virtual std::vector<int> getRelatedNodes(int node_id,int ws_id, int priority);
    virtual endeffector_id getEndEffectorFromNode(node_id id) const;
    virtual workspace_id getWsFromNode(node_id id) const;
    
    void getInfoFromNode(node_id id, endeffector_id& ee_id, grasp_id& grasp, workspace_id& workspace) const;
    bool setObject(object_id id, std::string name, int priority);
    void setEndEffectorMap(const std::map< int, const manipulation_planner* >& eeMap);
    void addFilteredArc(dual_manipulation_shared::planner_item source, dual_manipulation_shared::planner_item destination);
    Plan plan(grasp_id source_grasp_id, workspace_id source_workspace_id, grasp_id target_grasp_id, workspace_id target_workspace_id, std::vector< dual_manipulation_shared::planner_item >& path);
    std::string getName();
    ~manipulation_planner(){}
private:
    Object obj;
    std::map< int, const manipulation_planner* > eeMap;
};

#endif // MANIPULATION_PLANNER_H
