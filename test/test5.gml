Creator	"yFiles"
Version	"2.12"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"1"
		graphics
		[
			x	180.0
			y	270.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	1
		label	"2"
		graphics
		[
			x	180.0
			y	450.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	2
		label	"3"
		graphics
		[
			x	270.0
			y	360.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	3
		label	"4"
		graphics
		[
			x	390.0
			y	360.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	4
		label	"5"
		graphics
		[
			x	390.0
			y	450.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	5
		label	"6"
		graphics
		[
			x	390.0
			y	540.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	6
		label	"7"
		graphics
		[
			x	390.0
			y	270.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"7"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	7
		label	"8"
		graphics
		[
			x	390.0
			y	180.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	8
		label	"9"
		graphics
		[
			x	390.0
			y	90.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"9"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	9
		label	"10"
		graphics
		[
			x	180.0
			y	180.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"10"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	10
		label	"11"
		graphics
		[
			x	270.0
			y	90.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"11"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	11
		label	"12"
		graphics
		[
			x	300.0
			y	180.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"12"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	12
		label	"13"
		graphics
		[
			x	300.0
			y	270.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"13"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	13
		label	"14"
		graphics
		[
			x	510.0
			y	390.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"14"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	14
		label	"15"
		graphics
		[
			x	510.0
			y	270.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"15"
			fontSize	12
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	15
		label	"16"
		graphics
		[
			x	90.0
			y	360.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"16"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	16
		label	"17"
		graphics
		[
			x	90.0
			y	180.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"17"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	17
		label	"18"
		graphics
		[
			x	210.0
			y	30.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"18"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	18
		label	"19"
		graphics
		[
			x	390.0
			y	30.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"19"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	19
		label	"20"
		graphics
		[
			x	510.0
			y	150.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"20"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	20
		label	"21"
		graphics
		[
			x	600.0
			y	330.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"21"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	21
		label	"22"
		graphics
		[
			x	540.0
			y	510.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"22"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	node
	[
		id	22
		label	"23"
		graphics
		[
			x	270.0
			y	510.0
			w	30.0
			h	30.0
			type	"ellipse"
			fill	"#FFCC00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"23"
			fontSize	12
			fontName	"Dialog"
			model	"null"
		]
	]
	edge
	[
		source	0
		target	2
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	180.0
					y	270.0
				]
				point
				[
					x	270.0
					y	270.0
				]
				point
				[
					x	270.0
					y	360.0
				]
			]
		]
	]
	edge
	[
		source	1
		target	2
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	180.0
					y	450.0
				]
				point
				[
					x	270.0
					y	450.0
				]
				point
				[
					x	270.0
					y	360.0
				]
			]
		]
	]
	edge
	[
		source	2
		target	3
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	4
		target	3
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	5
		target	4
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	3
		target	6
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	6
		target	7
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	7
		target	8
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	0
		target	9
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	9
		target	10
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	180.0
					y	180.0
				]
				point
				[
					x	180.0
					y	90.0
				]
				point
				[
					x	270.0
					y	90.0
				]
			]
		]
	]
	edge
	[
		source	10
		target	11
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	270.0
					y	90.0
				]
				point
				[
					x	270.0
					y	180.0
				]
				point
				[
					x	300.0
					y	180.0
				]
			]
		]
	]
	edge
	[
		source	11
		target	12
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	12
		target	6
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	300.0
					y	270.0
				]
				point
				[
					x	345.0
					y	270.0
				]
				point
				[
					x	345.0
					y	269.0
				]
				point
				[
					x	390.0
					y	270.0
				]
			]
		]
		edgeAnchor
		[
			yTarget	-0.06666666666666667
		]
	]
	edge
	[
		source	4
		target	13
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	390.0
					y	450.0
				]
				point
				[
					x	510.0
					y	450.0
				]
				point
				[
					x	510.0
					y	390.0
				]
			]
		]
	]
	edge
	[
		source	13
		target	14
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	14
		target	7
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	510.0
					y	270.0
				]
				point
				[
					x	450.0
					y	270.0
				]
				point
				[
					x	450.0
					y	240.0
				]
				point
				[
					x	438.0
					y	240.0
				]
				point
				[
					x	438.0
					y	210.0
				]
				point
				[
					x	425.0
					y	210.0
				]
				point
				[
					x	425.0
					y	191.25
				]
				point
				[
					x	390.0
					y	180.0
				]
			]
		]
		edgeAnchor
		[
			yTarget	0.75
		]
	]
	edge
	[
		source	1
		target	15
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	180.0
					y	450.0
				]
				point
				[
					x	180.0
					y	360.0
				]
				point
				[
					x	90.0
					y	360.0
				]
			]
		]
	]
	edge
	[
		source	15
		target	16
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	16
		target	17
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	90.0
					y	180.0
				]
				point
				[
					x	90.0
					y	30.0
				]
				point
				[
					x	210.0
					y	30.0
				]
			]
		]
	]
	edge
	[
		source	17
		target	18
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	18
		target	19
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	390.0
					y	30.0
				]
				point
				[
					x	510.0
					y	30.0
				]
				point
				[
					x	510.0
					y	150.0
				]
			]
		]
	]
	edge
	[
		source	19
		target	20
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	510.0
					y	150.0
				]
				point
				[
					x	510.0
					y	240.0
				]
				point
				[
					x	555.0
					y	240.0
				]
				point
				[
					x	555.0
					y	330.0
				]
				point
				[
					x	600.0
					y	330.0
				]
			]
		]
	]
	edge
	[
		source	20
		target	21
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	600.0
					y	330.0
				]
				point
				[
					x	600.0
					y	510.0
				]
				point
				[
					x	540.0
					y	510.0
				]
			]
		]
	]
	edge
	[
		source	21
		target	5
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	540.0
					y	510.0
				]
				point
				[
					x	390.0
					y	510.0
				]
				point
				[
					x	390.0
					y	540.0
				]
			]
		]
	]
	edge
	[
		source	5
		target	22
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	390.0
					y	540.0
				]
				point
				[
					x	270.0
					y	540.0
				]
				point
				[
					x	270.0
					y	510.0
				]
			]
		]
	]
	edge
	[
		source	22
		target	1
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	270.0
					y	510.0
				]
				point
				[
					x	180.0
					y	510.0
				]
				point
				[
					x	180.0
					y	450.0
				]
			]
		]
	]
	edge
	[
		source	18
		target	8
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	19
		target	14
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
		]
	]
	edge
	[
		source	17
		target	10
		graphics
		[
			type	"bezier"
			fill	"#000000"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	210.0
					y	30.0
				]
				point
				[
					x	210.0
					y	90.0
				]
				point
				[
					x	270.0
					y	90.0
				]
			]
		]
	]
]
