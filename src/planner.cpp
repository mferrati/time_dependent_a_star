/*********************************************************************
 * 
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2016, Simone Nardi, Hamal Marino, Mirko Ferrati, Centro di Ricerca "E. Piaggio", University of Pisa
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the ISR University of Coimbra nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "time_dependent_a_star/planner.h"
#include <lemon/concepts/graph.h>

//  #define _PRINT_DEBUG_LOCKING
// #define _PRINT_DEBUG_
Plan Planner::unify(Plan a, Plan b)
{
    Plan result;
    for (Node& na:a.nodes)
    {
        for (Node& nb:b.nodes)
        {
            if (na.ee_id!=nb.ee_id) continue;
            if (na.ws_id<UNFEASIBLE_PATH_COST && nb.ws_id<UNFEASIBLE_PATH_COST && na.ws_id != nb.ws_id) continue;
            if ((nb.start_time<=na.start_time-na.father_cost && na.start_time-na.father_cost<=nb.end_time) ||
                (na.start_time<=nb.start_time-nb.father_cost && nb.start_time-nb.father_cost<=na.end_time))
            {
                na.start_time=std::min(na.start_time,nb.start_time);
                nb.start_time=std::min(na.start_time,nb.start_time);
                na.end_time=std::max(na.end_time,nb.end_time);
                nb.end_time=std::max(na.end_time,nb.end_time);
                na.father_cost=std::max(na.father_cost,nb.father_cost);
                nb.father_cost=std::max(na.father_cost,nb.father_cost);
            }
        }
    }
    result=a;
    result.priority=std::min(a.priority,b.priority);
    result.nodes.insert(result.nodes.begin(),b.nodes.begin(),b.nodes.end());
    return result;
}

////////////////////////////////////////////////////
Planner::Planner(lemon::SmartDigraph& graph)
: m_graph(graph)
, m_rev_graph( ReverseGraph(graph) )
, static_arc_map(graph)
, heuristic_alg(m_rev_graph, static_arc_map)
, dist_map( m_rev_graph )
, filter( graph, true )
{
  heuristic_alg.distMap(dist_map);
  
//   int arc_id = 0; 
//   std::cout << "removed arc " << graph.id(graph.source(graph.arcFromId(arc_id))) << " - " << graph.id(graph.target(graph.arcFromId(arc_id))) << std::endl;
//   filter[graph.arcFromId(arc_id)]=false;
}

////////////////////////////////////////////////////
void Planner::setStaticMap(const ArcCostMap& map_)
{
#ifdef _PRINT_DEBUG_
  std::cout << "Reset static map" << std::endl;
#endif
  
  for (lemon::SmartDigraph::ArcIt a(m_graph);a!=lemon::INVALID;++a)
  {
      static_arc_map[a]=map_[a];
#ifdef _PRINT_DEBUG_
      std::cout << "Arc " << m_graph.id(m_graph.source(a)) << "-" << m_graph.id(m_graph.target(a)) << " cost " << static_arc_map[a] << std::endl;
#endif
  }
  
  heuristic_alg = HeuristicAlg( m_rev_graph, static_arc_map);
  heuristic_alg.distMap(dist_map);
}

////////////////////////////////////////////////////
cost Planner::GetHeuristicCost(Graph::Node & test_, Graph::Node & target_)
{
  //This will just behave as Dijkstra instead of A*, but we can
#ifdef _PRINT_DEBUG_
   std::cout << " " << heuristic_alg.dist(test_) << std::endl;
#endif
  return heuristic_alg.dist(test_);
}

////////////////////////////////////////////////////
cost Planner::GetArcCost(Graph::Arc & out_arc_, time_m time_)
{
  cost dynamic_cost=0;
  if (dynamic_arc_map.count(m_graph.id(out_arc_)))
  try 
  {
      dynamic_cost = dynamic_arc_map.at(m_graph.id(out_arc_)).cost_map.at(time_);
  }
  catch(...)
  // because cost_map is limited on time, this way in case there are no other way!
  {
      dynamic_cost = UNFEASIBLE_PATH_COST;
  }
  return static_arc_map[out_arc_]+dynamic_cost;
}

////////////////////////////////////////////////////
void Planner::clearFilteredArcs()
{
    for ( Graph::ArcIt arc ( m_graph ); arc != lemon::INVALID; ++arc )
        filter[arc]= true;
 }

////////////////////////////////////////////////////
void Planner::addFilteredArc(Graph::Arc arc_)
{
    filter[arc_] = false;
}

////////////////////////////////////////////////////
Plan Planner::addObject(node_id source_, node_id target_, time_m t0_, uint16_t priority_)
{
#ifdef _PRINT_DEBUG_
    std::cout<< __func__ << " Source node " << source_<<std::endl;
    std::cout<< __func__ << " Target node " << target_<<std::endl;
    std::cout<< __func__ << " Initial time " << t0_<<std::endl;
#endif
    
  lemon::SmartDigraph::NodeMap<cost> l_cost_node_map(m_graph);
  lemon::SmartDigraph::NodeMap<char> l_search_node_map(m_graph, 'u');
  lemon::SmartDigraph::NodeMap<lemon::SmartDigraph::Node> l_predecessor_node_map(m_graph);
  
#ifdef _PRINT_DEBUG_
  std::cout<< __func__ << " Graph, number of nodes " << m_graph.nodeNum()<<std::endl;
  std::cout<< __func__ << " Graph, number of arcs " << m_graph.arcNum()<<std::endl;
#endif
  
  l_search_node_map[ m_graph.nodeFromId(source_) ]='l'; // labeled
  l_cost_node_map[ m_graph.nodeFromId(source_) ]= t0_;
  
  lemon::SmartDigraph::Node l_target = m_graph.nodeFromId(target_);
  
  // perform dijkstra to reach target node
  heuristic_alg.run(l_target);
#ifdef _PRINT_DEBUG_  
  for (Graph::ArcIt a(m_graph);a!=lemon::INVALID;++a)
  {
    if (filter[a]==false) 
      std::cout<<"arc "<<m_graph.id(a)<<" was filtered"<<std::endl;
  }
#endif
  FilterGraph l_subgraph(m_graph, filter);
 
  bool l_target_achieved = false;
  do
  {
    // let v be a labeled node with the smallest g(v) + h(v,g(v)).
    // In the case there are multiple candidates, choose one with the smallest g(v)
    cost g_v = std::numeric_limits<cost>::max();
    cost h_v = std::numeric_limits<cost>::max();
    lemon::SmartDigraph::Node v;
    bool l_node_found = false;
    for (lemon::SmartDigraph::NodeIt n(m_graph);n!=lemon::INVALID;++n)
    {
	// node_id curr_id = m_graph.id(n);
	//lemon::SmartDigraph::Node l_node = m_graph.nodeFromId(2);  
      
	if(l_search_node_map[n]  != 'l')
	  continue;
      
#ifdef _PRINT_DEBUG_
	std::cout << "Selected node " << m_graph.id(n) <<std::endl;
#endif
	cost g_n = l_cost_node_map[n];

#ifdef _PRINT_DEBUG_
 	std::cout << "Heuristic from " << m_graph.id(n) << " to " << target_ << " = ";
#endif
	cost h_n = GetHeuristicCost(n, l_target);
	
        
	if( (g_n + h_n < g_v + h_v) || 
	    ( g_n + h_n == g_v + h_v && g_n < g_v) )
	{
	  g_v = g_n;
	  h_v = h_n;
	  v = n;
	  l_node_found = true;
	}
    }
    
    if(!l_node_found)
    {
      ROS_ERROR("Next node not found");
      break; // ERROR!!! Next Node not found!
    }
    
    if(v == l_target)
    {
      l_target_achieved = true;
      break;
    }
    
    // for all out arc from v:
    for(FilterGraph::OutArcIt out_arc(l_subgraph, v); out_arc!= lemon::INVALID; ++out_arc)
    //for(Graph::OutArcIt out_arc(m_graph, v); out_arc!= lemon::INVALID; ++out_arc)
    {
      Graph::Node w = m_graph.target(out_arc);
      
      if(l_search_node_map[w] == 'f')
	continue;
      
#ifdef _PRINT_DEBUG_
      std::cout << "Selected target node " << m_graph.id(w) <<std::endl;
#endif
      
      cost l_cost = g_v + GetArcCost(out_arc, g_v);
      
      if(l_search_node_map[w] == 'u')
      {
	l_search_node_map[w] = 'l';
	l_cost_node_map[w] = l_cost;
	l_predecessor_node_map[w] = v;
      }
      else 
      {
	if(l_cost_node_map[w] > l_cost)
	{
	  l_cost_node_map[w] = l_cost;
	  l_predecessor_node_map[w] = v;
	}
      }
    }
    
    l_search_node_map[v] = 'f';
        
  }
  while(!l_target_achieved);
  
  if(!l_target_achieved)
  {
      ROS_ERROR("Target not reached!");
    // ERROR, no feasible plan!
    return Plan();
  }
  
  // collect path:
  lemon::SmartDigraph::Node l_source = m_graph.nodeFromId(source_);
  lemon::SmartDigraph::Node l_predecessor = l_target;
  Plan l_path;
  l_path.priority=priority_;
  Node n;
  n.id=m_graph.id(l_target);
  n.ee_id=getEndEffectorFromNode(n.id);
  n.ws_id=getWsFromNode(n.id);
  n.start_time=l_cost_node_map[l_target];
  n.end_time=n.start_time+static_arc_map[lemon::findArc(m_graph,l_predecessor_node_map[l_target],l_target)];
  n.father_id=m_graph.id(l_predecessor_node_map[l_target]);
  l_path.Front(n);
  l_predecessor = l_predecessor_node_map[l_target];
  lemon::SmartDigraph::Arc a = lemon::findArc<Graph>(m_graph, l_predecessor, l_target);
  n.father_cost= static_arc_map[a];
  
  while(l_predecessor != l_source)
  {
    lemon::SmartDigraph::Node l_current = l_predecessor;
    cost g_d = l_cost_node_map[l_current];
    l_predecessor = l_predecessor_node_map[l_current];
    Node n( m_graph.id(l_current), g_d, m_graph.id(l_predecessor));
    lemon::SmartDigraph::Arc a = lemon::findArc<Graph>(m_graph, l_predecessor, l_current);
    n.father_cost= static_arc_map[a];
    n.ee_id=getEndEffectorFromNode(n.id);
    n.ws_id=getWsFromNode(n.id);
    n.end_time=l_path.nodes.front().start_time;
    l_path.Front(n);
  }
  
  n.id=source_;
  n.ee_id=getEndEffectorFromNode(n.id);
  n.ws_id=getWsFromNode(n.id);
  n.start_time=t0_;
  n.father_cost=0;
  n.end_time=l_path.nodes.front().start_time;
  n.father_id=-1;
  l_path.Front(n);
  
  return l_path;
}

endeffector_id Planner::getEndEffectorFromNode(node_id id) const
{
//     if(id==3) return 2;
    return id;
}

workspace_id Planner::getWsFromNode(node_id id) const
{
    return UNFEASIBLE_PATH_COST;
}


std::map<int,int> related;

////////////////////////////////////////////////////
void Planner::initFakeStaticMap()
{
    ArcCostMap l_cost_map(m_graph);
    for (lemon::SmartDigraph::ArcIt a(m_graph);a!=lemon::INVALID;++a)
    {
        l_cost_map[a]=HEURISTIC_COST;
    }
//     l_cost_map[m_graph.arcFromId(2)]=7;
    
    setStaticMap(l_cost_map);
    
//     related[2] = 3;
//     related[3] = 2;
}

////////////////////////////////////////////////////
std::vector< int > Planner::getRelatedNodes(int node_id, int ws_id, int priority)
{
    std::vector<int> l_samenode(1,node_id);
    if(related.count(node_id))
    {
      int other = related[node_id];
      l_samenode.push_back( other  );
//       std::cout << "same node "<< node_id << ", " << other << std::endl;
    }
    
    return l_samenode;
}

////////////////////////////////////////////////////

void Planner::addLocking(const std::vector<Plan>& plans)
{
    for (auto plan:plans)
    {
        addLocking(plan);
    }
}

///
void Planner::addLocking(const Plan& plan)
{
    if (plan.priority>this->priority)
    {
        ROS_ERROR_STREAM("Ignored low-priority plan!!!");
        abort();
    }
    for (auto node:plan.nodes)
    {
	int l_father_time = 0;
// 	Graph::Node l_father_node = m_graph.nodeFromId(node.father_id);
// 	if(l_father_node != lemon::INVALID)
// 	{
// 	  lemon::SmartDigraph::Arc a = lemon::findArc<Graph>(m_graph, l_father_node, m_graph.nodeFromId(node.id));
// 	  if(a != lemon::INVALID)
// 	    l_father_time = static_arc_map[a];
// 	}
        l_father_time=node.father_cost;
	std::vector<int> related_nodes=getRelatedNodes(node.ee_id,node.ws_id,plan.priority);
        for (int i=0; i<related_nodes.size(); i++)
        {
#ifdef _PRINT_DEBUG_LOCKING
	std::cout << "Locking node " << related_nodes[i] << std::endl;
#endif
	    std::vector<lemon::SmartDigraph::Arc> l_arcs;
	    for (lemon::SmartDigraph::InArcIt a(m_graph,m_graph.nodeFromId(related_nodes[i]));a!=lemon::INVALID;++a)
	      l_arcs.push_back(a);
	    for (lemon::SmartDigraph::OutArcIt a(m_graph,m_graph.nodeFromId(related_nodes[i]));a!=lemon::INVALID;++a)
	      l_arcs.push_back(a);
	    
	    for(size_t arc_index = 0; arc_index < l_arcs.size(); ++arc_index)
	    {
		lemon::SmartDigraph::Arc& a = l_arcs[arc_index];
		int local_cost = static_arc_map[a];
                int id=m_graph.id(a);
#ifdef _PRINT_DEBUG_LOCKING
 	std::cout << "arc " << m_graph.id(m_graph.source(a)) << "-" << m_graph.id(m_graph.target(a)) <<std::endl;
#endif
                dynamic_arc_map[id].cost_map.resize(MAX_TIME);
                int start_time = (int)node.start_time-(int)l_father_time;
                int end_time = node.end_time; 
		
#ifdef _PRINT_DEBUG_LOCKING
		std::cout << "Start time " << start_time << " end time " << end_time ;
#endif
		
		for (int j=start_time-local_cost;(j<MAX_TIME) && (j<start_time);j++)
                {
                    if (j<0) continue;
		    
                    dynamic_arc_map[id].cost_map[j]=
		      std::max(int(dynamic_arc_map[id].cost_map[j]), end_time - j );
#ifdef _PRINT_DEBUG_LOCKING
 	std::cout << "(" << j << "," << dynamic_arc_map[id].cost_map[j] << ") ";
#endif
                }
		
                for (int j=start_time;(j<MAX_TIME) && (j<end_time);j++)
                {
                    if (j<0) continue;
		    
                    dynamic_arc_map[id].cost_map[j]=
		      std::max(int(dynamic_arc_map[id].cost_map[j]), end_time - j); //int( end_time - floor(double(j-start_time) * double(end_time-j)/double(end_time-start_time))) - j );
#ifdef _PRINT_DEBUG_LOCKING
 	std::cout << "(" << j << "," << dynamic_arc_map[id].cost_map[j] << ") ";
#endif
                }
#ifdef _PRINT_DEBUG_LOCKING
 	std::cout << std::endl;
#endif
	    }
	}
    }
}


void Planner::clearLocking()
{
    dynamic_arc_map.clear();
}
